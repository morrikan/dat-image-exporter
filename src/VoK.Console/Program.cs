﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Newtonsoft.Json;
using VoK.Common;
using VoK.DatUtil;

namespace VoK.Console
{
    class Program
    {
        private static Settings _settings;
        private static string _settingsFile;

        static void Main(string[] args)
        {
            _settingsFile = Path.Combine(".", "settings.json");

            if (File.Exists(_settingsFile))
                _settings = JsonConvert.DeserializeObject<Settings>(File.ReadAllText(_settingsFile));
            else
                _settings = new Settings();

            while (RunMainMenu()) { }
        }

        private static void SearchForInstallationFolders()
        {
            System.Console.WriteLine("Searching for installations.  Please wait, this may take a few minutes...");

            var root = Directory.GetDirectoryRoot(".");
            _settings.InstallationDirectories = new List<string>();
            SearchInDirectory(root, _settings.InstallationDirectories);

            // save the results
            SaveSettings();

            System.Console.WriteLine("\r" + Environment.NewLine);

            if (_settings.InstallationDirectories.Count == 0)
            {
                System.Console.WriteLine("No installation folders found.  The Vault of Kundarak Extractor must be on the same drive as your installation directory.");
                return;
            }

            System.Console.WriteLine("Installation Directories found:");

            for (int i = 0; i < _settings.InstallationDirectories.Count; i++)
            {
                System.Console.WriteLine($" {i + 1}) {_settings.InstallationDirectories[i]}");
            }

            System.Console.WriteLine();
        }

        private static void SearchInDirectory(string path, List<string> matches)
        {
            try
            {
                int len = Math.Min(65, path.Length);
                System.Console.Write($"\r  Searching {path.Substring(0, len)}");

                string cg = Path.Combine(path, "dndclient.exe");
                if (File.Exists(cg))
                    matches.Add(path);

                Directory.GetDirectories(path)
                    .ToList()
                    .ForEach(s => SearchInDirectory(s, matches));
            }
            catch (UnauthorizedAccessException ex)
            {
                // ok, so we are not allowed to dig into that directory. Move on.
            }
            catch (Exception ex)
            {
                // no really, just skip this directory
            }
        }

        private static bool RunMainMenu()
        {
            if (_settings.InstallationDirectories.Count > 0)
            {
                System.Console.WriteLine("Your current installation directories:");

                for (int i = 0; i < _settings.InstallationDirectories.Count; i++)
                {
                    System.Console.WriteLine($"  {i + 1}) {_settings.InstallationDirectories[i]}");
                }
            }

            System.Console.WriteLine("");
            System.Console.WriteLine("Vault of Kundarak Dat Extractor - Main Menu");
            System.Console.WriteLine("  1) Add Installation Directories");
            System.Console.WriteLine("  2) Extract client_surface.dat");
            System.Console.WriteLine("  3) Extract client_highres.dat");
            System.Console.WriteLine("  4) Extract client_sound.dat");
            System.Console.WriteLine("  5) Generate Readme image previews");
            System.Console.WriteLine("  6) Select specific dat file");
            System.Console.WriteLine("  7) Generate HTML pages for S3");
            System.Console.WriteLine("  8) Export Icons");
            System.Console.WriteLine("  Q) Quit");

            string input = System.Console.ReadLine();

            if (string.IsNullOrWhiteSpace(input))
            {
                System.Console.WriteLine("Select an option.");
                return true;
            }

            switch (input.ToLower())
            {
                case "1":
                    EnterInstallationDirectories();
                    // SearchForInstallationFolders();
                    break;
                case "2":
                    Export("client_surface.dat");
                    break;
                case "3":
                    Export("client_highres.dat");
                    break;
                case "4":
                    Export("client_sound.dat");
                    break;
                case "5":
                    GenerateWikiPages();
                    break;
                case "6":
                    ExportSpecificDatFile();
                    break;
                case "7":
                    GenerateHtmlPages();
                    break;
                case "8":
                    ExportIcons("client_surface.dat");
                    break;
                case "q":
                    return false;
            }

            return true;
        }

        private static bool ExportSpecificDatFile()
        {
            string filename = null;

            if (!SelectInstallationDirectory(out string folder))
                return false;

            var dats = Directory.GetFiles(folder, "*.dat");

            for (int i = 1; i <= dats.Length; i++)
                System.Console.WriteLine($" {i}) {dats[i - 1]}");

            string input = System.Console.ReadLine();

            if (!string.IsNullOrWhiteSpace(input) && int.TryParse(input, out int selection))
            {
                if (selection < 1 || selection > dats.Length)
                    return false;

                filename = dats[selection - 1];
            }
            else
            {
                return false;
            }

            try
            {
                ExportDat(folder, filename);
                return true;
            }
            catch (Exception ex)
            {
                System.Console.WriteLine("Error exporting dat: " + ex.ToString());
                return false;
            }
        }

        private static void Export(string filename)
        {
            if (!SelectInstallationDirectory(out string folder))
                return;

            try
            {
                ExportDat(folder, filename);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine("Error exporting dat: " + ex.ToString());
            }
        }

        private static void ExportIcons(string filename)
        {
            if (!SelectInstallationDirectory(out string folder))
                return;

            try
            {
                ExportIcons(folder, filename);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine("Error exporting icons: " + ex.ToString());
            }
        }

        private static bool SelectInstallationDirectory(out string folder)
        {
            folder = null;

            if (_settings.InstallationDirectories.Count < 1)
            {
                System.Console.WriteLine("No installation directories found.");
                return false;
            }

            System.Console.WriteLine("Select an installation directory:");

            for (int i = 0; i < _settings.InstallationDirectories.Count; i++)
            {
                System.Console.WriteLine($" {i + 1}) {_settings.InstallationDirectories[i]}");
            }

            string input = System.Console.ReadLine();

            if (!string.IsNullOrWhiteSpace(input) && int.TryParse(input, out int selection))
            {
                if (selection > 0 && selection <= _settings.InstallationDirectories.Count)
                {
                    // valid input
                    folder = _settings.InstallationDirectories[selection - 1];
                    return true;
                }
            }

            System.Console.WriteLine("Please make a valid selection.");
            return false;
        }

        private static void EnterInstallationDirectories()
        {
            System.Console.WriteLine("Enter new installation directories, 1 per line.  Enter an empty line when done.");

            while (true)
            {
                string input = System.Console.ReadLine();

                if (string.IsNullOrWhiteSpace(input))
                    break;

                if (!_settings.InstallationDirectories.Contains(input)
                    && Directory.Exists(input)
                    && File.Exists(Path.Combine(input, "dndclient.exe")))
                    _settings.InstallationDirectories.Add(input);
                else
                    System.Console.WriteLine("Unable to validate specified directory.");
            }

            SaveSettings();
        }

        private static bool SelectExtractDirectory(out string extractDirectory)
        {
            string current = _settings.OutputFolder;
            extractDirectory = null;

            if (string.IsNullOrWhiteSpace(current))
                current = Directory.GetCurrentDirectory();

            System.Console.WriteLine($"Current Extraction directory:");
            System.Console.WriteLine($"{current}");

            System.Console.WriteLine($"Enter new directory or leave blank to use the current directory:");
            string input = System.Console.ReadLine();

            if (string.IsNullOrWhiteSpace(input))
            {
                input = current;
            }
            else
            {
                if (!Directory.Exists(input))
                {
                    System.Console.WriteLine("Unable to validate directory.");
                    return false;
                }
            }

            _settings.OutputFolder = input;
            extractDirectory = input;
            SaveSettings();

            return true;
        }

        private static void SaveSettings()
        {
            string json = JsonConvert.SerializeObject(_settings);
            File.WriteAllText(_settingsFile, json);
        }

        private static void GenerateWikiPages()
        {
            if (!SelectExtractDirectory(out string outputFolder))
                return;

            System.Console.WriteLine("Generating wiki page for " + outputFolder);

            GenerateMarkdownForEachFolder(outputFolder, "client_surface.dat\\RenderSurface", "client_surface.dat\\RenderSurface");
            GenerateMarkdownForEachFolder(outputFolder, "client_highres.dat\\RenderSurface", "client_highres.dat\\RenderSurface");
        }

        private static void GenerateHtmlPages()
        {
            if (!SelectExtractDirectory(out string outputFolder))
                return;

            System.Console.WriteLine("Generating html pages for " + outputFolder);

            GenerateHtmlPagesForFolder(outputFolder, "client_surface.dat", Path.Combine(outputFolder, "client_surface.dat\\RenderSurface"));
            GenerateHtmlPagesForFolder(outputFolder, "client_highres.dat", Path.Combine(outputFolder, "client_highres.dat\\RenderSurface"));
        }

        private static void GenerateMarkdownForEachFolder(string sourceFolder, string subfolder, string outputFolder)
        {
            string fullPath = Path.Combine(sourceFolder, subfolder);

            var allDirs = Directory.EnumerateDirectories(fullPath);
            foreach (var dir in allDirs)
                GenerateMarkdownForEachFolder(sourceFolder, dir.Replace(sourceFolder + "\\", ""), outputFolder);

            if (!string.IsNullOrWhiteSpace(subfolder))
                GenerateSingleMarkdownPage(sourceFolder, fullPath.Replace(sourceFolder + "\\", ""), outputFolder);
        }

        private static void GenerateSingleMarkdownPage(string sourceFolder, string subFolder, string outputFolder)
        {
            if (sourceFolder.EndsWith("\\"))
                sourceFolder.TrimEnd('\\');

            var subDir = Path.Combine(sourceFolder, subFolder);
            var allFiles = Directory.EnumerateFiles(subDir, "*", SearchOption.TopDirectoryOnly);

            StringBuilder sb = new StringBuilder();

            string relativeUrl = subFolder.Replace("\\", "/");
            string absUrl = "https://gitlab.com/VaultOfKundarak/DDO-export/raw/master/content";

            foreach (var file in allFiles)
            {
                var fileUrl = file.Replace(sourceFolder, absUrl).Replace("\\", "/");
                sb.AppendLine($"![logo]({fileUrl})");
            }

            if (sb.Length < 10)
                return;

            string readmeName = subFolder.Substring(subFolder.LastIndexOf("\\") + 1) + ".md";
            string relative = Path.Combine(outputFolder, readmeName);
            string readmePath = Path.Combine(sourceFolder, relative);

            System.Console.WriteLine($"Writing {relative} ({sb.Length} bytes)...");
            File.WriteAllText(readmePath, sb.ToString());
        }

        private static void GenerateHtmlPagesForFolder(string absRootFolder, string relativeOutputFolder, string relativeContentFolder)
        {
            string fullPath = Path.Combine(absRootFolder, relativeContentFolder);

            var allDirs = Directory.EnumerateDirectories(fullPath).ToList();
            var contentDirs = new List<string>();

            string previous = "";
            string next = "";

            foreach(var dir in allDirs)
            {   
                // top level contains a few "0x4100" type folders
                var subDirs = Directory.EnumerateDirectories(dir).ToList();
                contentDirs.AddRange(subDirs);
            }

            for (int i = 0; i < contentDirs.Count; i++)
            {
                var dir = contentDirs[i].Replace(absRootFolder + "\\", "");
                if (i < contentDirs.Count - 1)
                    next = contentDirs[i + 1].Replace(absRootFolder + "\\", "");

                GenerateSingleHtmlPage(absRootFolder, relativeOutputFolder, dir, previous, next);

                previous = dir;
            }

            EmitCssFile(Path.Combine(absRootFolder, relativeOutputFolder));
            GenerateIndexHtml(absRootFolder, relativeOutputFolder, contentDirs);

            //if (!string.IsNullOrWhiteSpace(subfolder))
            //    GenerateSingleHtmlPage(absRootFolder, relativeOutputFolder, relativeContentFolder);
        }

        private static void GenerateIndexHtml(string absRootFolder, string relativeOutputFolder, List<string> contents)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<html>");
            sb.AppendLine("<head><title>Vault of Kundarak Dat Export</title>");
            sb.AppendLine("<link rel=\"stylesheet\" href=\"site.css\">");
            sb.AppendLine("</head><body>");

            foreach(string file in contents)
            {
                string contentFile = GetHtmlPageName(file);
                string path = relativeOutputFolder + "/" + contentFile;
                sb.AppendLine($"<a href=\"{path}\">{contentFile}</a><br/>");
            }

            sb.AppendLine("</body></html>");

            string filename = relativeOutputFolder + ".html";
            string filepath = Path.Combine(absRootFolder, filename);

            System.Console.WriteLine($"Writing {filename} ({sb.Length} bytes)...");
            File.WriteAllText(filepath, sb.ToString());
        }

        private static void EmitCssFile(string outputFolder)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(".imgFrame { width: 200px; display: inline; }");
            sb.AppendLine("img { max-width: 200px; max-height: 200px; }");

            File.WriteAllText(outputFolder + "\\site.css", sb.ToString());
        }

        private static void GenerateSingleHtmlPage(string absRootFolder, string relativeOutputFolder, string relativeContentFolder, string previous, string next)
        {
            if (relativeOutputFolder.EndsWith("\\"))
                relativeOutputFolder.TrimEnd('\\');

            var subDir = Path.Combine(absRootFolder, relativeContentFolder);
            var allFiles = Directory.EnumerateFiles(subDir, "*", SearchOption.TopDirectoryOnly).ToList();
            var relativeUrl = Path.Combine(absRootFolder, relativeOutputFolder);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<html>");
            sb.AppendLine("<head><title>Vault of Kundarak Dat Export</title>");
            sb.AppendLine("<link rel=\"stylesheet\" href=\"site.css\">");
            sb.AppendLine("</head><body>");

            string pagination = "";

            if (!string.IsNullOrWhiteSpace(previous))
                pagination += $"<a href=\"{GetHtmlPageName(previous)}\">[previous page]</a>&nbsp;";

            if (!string.IsNullOrWhiteSpace(next))
                pagination += $"<a href=\"{GetHtmlPageName(next)}\">[next page]</a>";

            if (!string.IsNullOrWhiteSpace(pagination))
            {
                sb.AppendLine(pagination);
                sb.AppendLine("<br/>");
            }

            if (allFiles.Count < 1)
                return;

            for (int i = 0; i < allFiles.Count; i++)
            {
                var file = allFiles[i];
                var fileName = file.Substring(file.LastIndexOf("\\") + 1);
                var fileUrl = file.Replace(relativeUrl, "").Replace("\\", "/");
                if (fileUrl.StartsWith("/"))
                    fileUrl = fileUrl.Substring(1);
                sb.AppendLine($"<div class=\"imgFrame\"><a href=\"{fileUrl}\"><img src=\"{fileUrl}\" alt=\"{fileName}\"/></a></div>");

                if ((i + 1) % 6 == 0)
                    sb.AppendLine("<br/>");
            }

            if (!string.IsNullOrWhiteSpace(pagination))
            {
                sb.AppendLine("<br/>");
                sb.AppendLine(pagination);
            }

            sb.AppendLine("</body></html>");
            
            string pageName = GetHtmlPageName(relativeContentFolder);
            string relative = Path.Combine(relativeOutputFolder, pageName);
            string readmePath = Path.Combine(absRootFolder, relative);

            System.Console.WriteLine($"Writing {relative} ({sb.Length} bytes)...");
            File.WriteAllText(readmePath, sb.ToString());
        }

        private static string GetHtmlPageName(string relativeContentFolder)
        {
            string readmeName = relativeContentFolder.Substring(relativeContentFolder.LastIndexOf("\\") + 1) + ".html";
            return readmeName;
        }

        private static void ExportDat(string path, string datFileName)
        {
            if (!SelectExtractDirectory(out string outputFolder))
                return;

            System.Console.WriteLine("Exporting to:");
            System.Console.WriteLine(outputFolder);

            DatFile datFile = DatFile.LoadFromDatFile(Path.Combine(path, datFileName));

            string directory = outputFolder + "\\" + datFileName + "\\";

            System.Console.WriteLine("Checking for export directory structure..." + directory);

            if (!Directory.Exists(directory))
            {
                System.Console.WriteLine("Creating export directory structure...");
                System.Console.Out.Flush();
                Directory.CreateDirectory(directory);
            }

            var copyOfFiles = datFile.FileList.ToList();

            System.Console.WriteLine($"Beginning export of {copyOfFiles.Count} files.");

            foreach (var df in copyOfFiles)
            {
                byte[] data;
                string fileExtension;
                string folder = "unknown";
                bool isIcon = false;

                try
                {

                    data = Ddo.ContentConverter.ExportContent(datFile, df.Id, out fileExtension, out folder, out isIcon);

                    if (data == null)
                        continue;

                    string subfolder = df.Id.ToString("X8").Substring(0, 4);
                    string secondSubFolder = df.Id.ToString("X8").Substring(0, 6);
                    string typeDir = directory + folder + "\\0x" + subfolder + "\\0x" + secondSubFolder + "\\";

                    if (!Directory.Exists(typeDir))
                        Directory.CreateDirectory(typeDir);

                    string filebase = typeDir + df.Id.ToString("X8");
                    string binaryFilename = filebase + fileExtension;
                    File.WriteAllBytes(binaryFilename, data);
                    System.Console.WriteLine($"{binaryFilename} - {data.Length} bytes written.");

                }
                catch (Exception ex)
                {
                    System.Console.WriteLine($"Error exporting {df.Id}: {ex}");
                    continue;
                }
            }
        }

        private static void ExportIcons(string path, string datFileName)
        {
            if (!SelectExtractDirectory(out string outputFolder))
                return;

            System.Console.WriteLine("Exporting to:");
            System.Console.WriteLine(outputFolder);

            DatFile datFile = DatFile.LoadFromDatFile(Path.Combine(path, datFileName));

            string directory = outputFolder + "\\";

            System.Console.WriteLine("Checking for export directory structure..." + directory);

            if (!Directory.Exists(directory))
            {
                System.Console.WriteLine("Creating export directory structure...");
                System.Console.Out.Flush();
                Directory.CreateDirectory(directory);
            }

            var copyOfFiles = datFile.FileList.ToList();

            System.Console.WriteLine($"Beginning export of {copyOfFiles.Count} files.");

            foreach (var df in copyOfFiles)
            {
                byte[] data;
                string fileExtension;
                string folder = "unknown";
                bool isIcon = false;

                try
                {

                    data = Ddo.ContentConverter.ExportContent(datFile, df.Id, out fileExtension, out folder, out isIcon);

                    if (data == null || !isIcon)
                        continue;

                    string subfolder = df.Id.ToString("X8").Substring(0, 5);
                    string typeDir = directory + "\\0x" + subfolder + "\\";

                    if (!Directory.Exists(typeDir))
                        Directory.CreateDirectory(typeDir);

                    string filebase = typeDir + df.Id.ToString("X8");
                    string binaryFilename = filebase + fileExtension;
                    File.WriteAllBytes(binaryFilename, data);
                    System.Console.WriteLine($"{binaryFilename} - {data.Length} bytes written.");

                }
                catch (Exception ex)
                {
                    System.Console.WriteLine($"Error exporting {df.Id}: {ex}");
                    continue;
                }
            }
        }
    }
}
