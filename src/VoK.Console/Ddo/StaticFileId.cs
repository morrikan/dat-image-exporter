﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoK.Ddo
{
    internal enum StaticFileId : uint
    {
        // these are for the network compression dictionaries
        Server2ClientDeferred   = 0x01000001,
        Client2Server           = 0x01000002,
        Server2Client           = 0x01000003,

        TypesIndex              = 0x28000000,

        ACTIONMAP               = 0x00000009,  // 2800000a.bin

        COMPRESSIONDICTIONARY   = 0x00000013,  // 28000018.bin
        CONTENT_LAYER           = 0x00000015,  // 2800001e.bin
        DBPC                    = 0x00000014,  // 2800001a.bin
        EMAPPER                 = 0x00000001,  // 28000001.bin
        FEAT                    = 0x10000002,  // 28000013.bin
        FONT                    = 0x0000000a,  // 2800000b.bin
        GLOSSARY_TERM           = 0x10000004,  // 2800001f.bin
        KEYMAP                  = 0x0000000b,  // 2800000c.bin
        MUSIC                   = 0x00000006,  // 28000005.bin
        PLAYERPHYSICSCONTROLLER = 0x0000000f,  // 28000006.bin
        PRELOADRESOURCES        = 0x00000012,  // 28000014.bin
        REGION                  = 0x00000007,  // 28000008.bin
        RIPCORDS                = 0x10000006,  // 28000029.bin
        SCRIPTCALLBACK          = 0x00000010,  // 28000007.bin
        SOUND                   = 0x10000005,  // 28000005.bin
        STRINGTABLE             = 0x00000004,  // 28000004.bin
        UILAYOUT                = 0x00000003,  // 28000003.bin
        UNDEFINED               = 0x00000000,
        UNIQUEDB                = 0x00000002,  // 28000002.bin
        WEENIECONTENT           = 0x0000000e,  // 2800000e.bin
        WLIB                    = 0x0000000d,  // 2800000d.bin
    }
}
