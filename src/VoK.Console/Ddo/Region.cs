﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoK.Ddo
{
    /// <summary>
    /// A list of known property descriptors for 28000008.bin - copied from Sif
    /// </summary>
    public enum REGION : uint
    {
        Undef               = 0x00000000,
        Stormreach          = 0x00000001,  // 0f000000.bin
        ForgottenRealms     = 0x00000002,  // 0f000001.bin
    }
}
