﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using VoK.Common;
using VoK.DatUtil;

namespace VoK.Ddo
{
    internal static class ObjectTypeHelper
    {
        public static ObjectType GetObjectType(int fileId)
        {
            foreach (ObjectType ot in Enum.GetValues(typeof(ObjectType)))
            {
                MemberInfo memberInfo = typeof(ObjectType).GetMember(ot.ToString()).FirstOrDefault();
                var attribs = memberInfo?.GetCustomAttributes(typeof(IdRangeAttribute)).Cast<IdRangeAttribute>().ToList();

                foreach(var attrib in attribs)
                {
                    if (attrib.MinValue <= fileId && attrib.MaxValue >= fileId)
                        return ot;
                }
            }

            return ObjectType.Unknown;
        }
    }
}
