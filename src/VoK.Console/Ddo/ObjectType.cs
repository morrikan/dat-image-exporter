﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using VoK.Common;

namespace VoK.Ddo
{
    /// <summary>
    /// Description attributes are derived from the values in enum 0x23000079
    /// </summary>
    public enum ObjectType
    {
        [Description("Invalid")]
        Invalid                 = 0,

        [Description("LANDBLOCKDATA")]
        [IdRange(0x80000000, 0x800FFFFF)]
        LandblockData           = 1,

        [Description("LANDBLOCKINFO")]
        [IdRange(0x80200000, 0x802FFFFF)]
        LandblockInfo           = 2,
        
        [Description("CELLMESH")]
        [IdRange(0x01000000, 0x01FFFFFF)]
        CellMesh                = 4,
        
        [Description("SCENE")]
        [IdRange(0x02000000, 0x02FFFFFF)]
        Scene                   = 5,
        
        [Description("SETUP")]
        [IdRange(0x04000000, 0x04FFFFFF)]
        Setup                   = 7,
        
        [Description("DBANIMATOR")]
        [IdRange(0x05000000, 0x05FFFFFF)]
        DbAnimator              = 8,
        
        [Description("REGION")]
        [IdRange(0x0F000000, 0x0FFFFFFF)]
        Region                  = 10,
        
        [Description("SCENE_DESC")]
        [IdRange(0x11000000, 0x11FFFFFF)]
        SceneDescription        = 12,

        [Description("TERRAIN_DESC")]
        [IdRange(0x12000000, 0x12FFFFFF)]
        Terrain                 = 13,

        [Description("ENCOUNTER_DESC")]
        [IdRange(0x14000000, 0x14FFFFFF)]
        EncounterDescription    = 15,

        [Description("PROPERTY_DESC")]
        [IdRange(0x18000000, 0x18FFFFFF)]
        PropertyDescription     = 17,

        [Description("SKY_DESC")]
        [IdRange(0x15000000, 0x15FFFFFF)]
        SkyDescription          = 16,

        [Description("WATER_DESC")]
        [IdRange(0x16000000, 0x16FFFFFF)]
        WaterDescription        = 18,

        [Description("FOG_DESC")]
        [IdRange(0x17000000, 0x17FFFFFF)]
        FogDescription          = 19,

        [Description("DAY_DESC")]
        [IdRange(0x1A000000, 0x1AFFFFFF)]
        DayDescription          = 21,

        [Description("DBPROPERTIES")]
        [IdRange(0x78000000, 0x7FFFFFFF)]
        DbProperties            = 23,
        
        [Description("VISUAL_DESC")]
        [IdRange(0x1F000000, 0x1FFFFFFF)]
        VisualDescription       = 25,

        [Description("APPEARANCE")]
        [IdRange(0x20000000, 0x20FFFFFF)]
        Appearance              = 26,
        
        [Description("UI_LAYOUT")]
        [IdRange(0x22000000, 0x22FFFFFF)]
        UiLayout                = 28,
        
        [Description("STRING_TABLE")]
        [IdRange(0x25000000, 0x26FFFFFF)]
        StringTable             = 29,
        
        [Description("ENUM_MAPPER")]
        [IdRange(0x23000000, 0x23FFFFFF)]
        EnumMapper              = 31,

        [Description("DID_MAPPER")]
        [IdRange(0x28000000, 0x28FFFFFF)]
        DidMapper               = 33,

        [Description("SOUNDINFO")]
        [IdRange(0x2A000000, 0x2AFFFFFF)]
        SoundInfo               = 35,

        [Description("RENDERMATERIAL")]
        [IdRange(0x2B000000, 0x2BFFFFFF)]
        RenderMaterial          = 37,

        [Description("MATERIALMODIFIER")]
        [IdRange(0x30000000, 0x30FFFFFF)]
        MaterialModifier        = 39,

        [Description("MATERIALINSTANCE")]
        [IdRange(0x31000000, 0x31FFFFFF)]
        MaterialInstance        = 40,

        [Description("LBO")]
        LBO                     = 45,

        [Description("RENDERSURFACE")]
        [IdRange(0x41000000, 0x41FFFFFF)]
        RenderSurface           = 51,

        [Description("RENDERTEXTURE")]
        [IdRange(0x40000000, 0x40FFFFFF)]
        RenderTexture           = 52,

        [Description("FONT")]
        [IdRange(0x42000000, 0x42000FFF)]
        Font                    = 53,
        
        [Description("KEYMAP")]
        [IdRange(0x1D000000, 0x1DFFFFFF)]
        KeyMap                  = 54,

        [Description("MASTER_PROPERTY")]
        [IdRange(0x34000000, 0x34FFFFFF)]
        MasterProperty          = 57,

        [Description("GAME_TIME")]
        [IdRange(0x35000000, 0x35FFFFFF)]
        GameTime                = 58,
        
        [Description("FILE2ID_TABLE")]
        [IdRange(0x0E000003, 0x0E000003)]
        FileToIdTable           = 61,

        [Description("PSDESC")]
        [IdRange(0x39000000, 0x39FFFFFF)]
        PSDescription           = 62,
        
        [Description("ENTITYDESC")]
        [IdRange(0x47000000, 0x47FFFFFF)]
        EntityDescription       = 63,

        [Description("ENTITYGROUP")]
        [IdRange(0x46000000, 0x46FFFFFF)]
        EntityGroup             = 64,
        
        [Description("UI_SCENE")]
        [IdRange(0x21000000, 0x21FFFFFF)]
        UiScene                 = 65,

        [Description("ACTIONMAP")]
        [IdRange(0x51000000, 0x51FFFFFF)]
        ActionMap               = 66,

        /// <summary>
        /// probably string table entries.  overlaps with a lot of things like feats, weenie content, and glossary terms
        /// </summary>
        [Description("WSTATE")]
        [IdRange(0x70000000, 0x77FFFFFF)]
        WState                  = 67,

        [Description("CONTENTDB")]
        [IdRange(0x53000000, 0x53FFFFFF)]
        ContentDb               = 68,
        
        [Description("SCRIPTTABLE")]
        [IdRange(0x07000000, 0x07FFFFFF)]
        ScriptTable             = 71,
        
        [Description("TABOO_TABLE")]
        [IdRange(0x0EBADA55, 0x0EBADA55)]
        TabooTable              = 72,

        [Description("WLIB")]
        [IdRange(0x56000000, 0x56FFFFFF)]
        Wlib                    = 73,

        [Description("PLACES_TABLE")]
        [IdRange(0x0E000004, 0x0E000004)]
        PlacesTable             = 75,
        
        /// <summary>
        /// this file is 101 bytes in client_local_{language} dat
        /// </summary>
        [Description("STRING_STATE")]
        [IdRange(0x58000000, 0x58000000)]
        StringStage             = 76,

        [Description("MAPNOTE_DESC")]
        [IdRange(0x0E000006, 0x0E00FFFF)]
        MapnoteDescription      = 79,
        
        [Description("CAMERASTATE")]
        [IdRange(0x08000000, 0x08FFFFFF)]
        CameraState             = 85,
        
        [Description("SCRIPTLET")]
        [IdRange(0x0C000000, 0x0CFFFFFF)]
        Scriptlet               = 87,
        
        [Description("SOUND")]
        [IdRange(0x0A000000, 0x0AFFFFFF)]
        Sound                   = 88,
        
        [Description("RENDER_MESH")]
        [IdRange(0x06000000, 0x06FFFFFF)]
        RenderMesh              = 90,
        
        [Description("PHYSICS_MESH")]
        [IdRange(0x66000000, 0x66FFFFFF)]
        PhysicsMesh             = 91,
        
        [Description("SCRIPTCHANNELS")]
        [IdRange(0x43001000, 0x43FFFFFF)]
        ScriptChannels          = 93,

        [Description("RENDER_LOD_CLASSES")]
        [IdRange(0x32000000, 0x32FFFFFF)]
        RenderLodClasses        = 94,
        
        [Description("FARVIEWENTITIES")]
        [IdRange(0x0E100001, 0x0E100001)]
        FarViewEntities         = 95,

        [Description("MONITOREDPROPERTIES")]
        [IdRange(0x0E100010, 0x0E100030)]
        MonitoredProperties     = 96,

        [Description("NAME_FILTER_TABLE")]
        [IdRange(0x0E200000, 0x0E20001F)]
        NameFilterTable         = 97,

        [Description("MOVIE")]
        [IdRange(0x0D000000, 0x0DFFFFFF)]
        Movie                   = 98,

        [Description("LANDBLOCKBLOCKMAP")]
        [IdRange(0x80100000, 0x801FFFFF)]
        LandblockBlockMap       = 99,

        [Description("IMAGE_COMPOSITOR")]
        [IdRange(0x33000000, 0x33FFFFFF)]
        ImageCompositor         = 100,
        
        [Description("TERRAIN_TYPE_TABLE")]
        [IdRange(0x19000000, 0x19FFFFFF)]
        TerrainTypeTable        = 103,

        [Description("LAND_BLOCK_TERRAIN_LIST")]
        [IdRange(0x24000000, 0x24FFFFFF)]
        LandblockTerrainList    = 104,
        
        [Description("COMPRESSION_DICTIONARY")]
        [IdRange(0x0E010000, 0x0E01FFFF)]
        CompressionDictionary   = 105,

        [Description("QUEST_MAPNOTE_DESC")]
        [IdRange(0xE4000000, 0xE4FFFFFF)]
        QuestMapnoteDescription = 107,

        [Description("RENDER_IMPOSTER_TABLE")]
        [IdRange(0x27000000, 0x27FFFFFF)]
        RenderImposterTable     = 108,

        [Description("DISTANT_LANDSCAPE_SECTOR")]
        [IdRange(0x80500000, 0x805FFFFF)]
        DistantLandscapeSector  = 109,

        [Description("STRING_TOKEN_MAPPER")]
        [IdRange(0x0E100003, 0x0E100003)]
        StringTokenMapper       = 110,

        [Description("KYNMAP")]
        [IdRange(0x80600000, 0x806FFFFF)]
        KynMap                  = 111,

        [Description("KYNGRAPH")]
        [IdRange(0x90000000, 0x9FFFFFFF)]
        KynGraph                = 112,
        
        [Description("SEGMENT_TABLE")]
        [IdRange(0x0E000002, 0x0E000002)]
        SegmentTable            = 113,

        [Description("LANDBLOCKPROPERTIES")]
        [IdRange(0x80400000, 0x804FFFFF)]
        LandblockProperties     = 114,

        [Description("FALLBACK_MAPPER_TABLE")]
        [IdRange(0x0E100009, 0x0E100009)]
        FallbackMapperTable     = 115,

        [Description("HKX")]
        HKX                     = 117,

        [Description("COMPUTESHADER")]
        [IdRange(0x2D000000, 0x2D0FFFFF)]
        ComputeShader           = 119,

        /// <summary>
        /// placeholder for anything missing
        /// </summary>
        [Description("Unknown")]
        Unknown                 = 0xFF
    }
}
