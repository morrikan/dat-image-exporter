﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoK.Ddo
{
    /// <summary>
    /// export copied from Sif
    /// </summary>
    public enum ContentLayer : uint
    {
        Undef                   = 0x00000000,
        InstanceZero            = 0x00000001,  // 780002f9.bin
        All                     = 0x00000002,  // 780002f9.bin
        test_npe_area           = 0x10000001,  // 780002f9.bin
        npe_village_thawed      = 0x10000002,  // 780002f9.bin
        npe_village_frozen      = 0x10000003,  // 78000359.bin
        PublicInstanceLayer     = 0x10000004,  // 780002f9.bin
        npe_village_thawed_b    = 0x10000005,  // 780002f9.bin
        PublicLayer             = 0x10000008,  // 780002f9.bin
        TestLayer               = 0x10000009,  // 780002f9.bin
        airship                 = 0x1000000a,  // 780002f9.bin
        droaam_lowbie           = 0x1000000d,  // 780002f9.bin
        droaam_part_one         = 0x1000000c,  // 780002f9.bin
        droaam_part_two         = 0x1000000b,  // 780002f9.bin
    }
}
