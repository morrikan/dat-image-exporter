﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
// using SixLabors.ImageSharp;
using VoK.Common;
using VoK.DatUtil;

namespace VoK.Ddo
{
    internal class ContentConverter
    {
        public static byte[] ExportContent(DatFile datFile, int fileId, out string fileExtension, out string folder, out bool isIcon)
        {
            ObjectType typeInfo = ObjectTypeHelper.GetObjectType(fileId);
            folder = typeInfo.ToString();

            // strip off the file id, it's not needed to parse content
            byte[] data = datFile.GetFileContents(fileId).Skip(4).ToArray();

            // default extension if we can't convert
            fileExtension = ".bin";
            isIcon = false;

            switch (typeInfo)
            {
                case ObjectType.RenderSurface:
                    data = ImageConverter.ConvertImage(fileId, data, out fileExtension, out ImageFormat format, out isIcon);
                    break;
                case ObjectType.Sound:
                    data = SoundConverter.Convert(fileId, data, out fileExtension);
                    break;
                case ObjectType.Movie:
                    break;
                default:

                    break;
            }
            
            return data;
        }
    }
}
