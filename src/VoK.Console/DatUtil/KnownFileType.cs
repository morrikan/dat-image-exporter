﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoK.DatUtil
{
    [Flags]
    internal enum KnownFileType : uint
    {
        Unknown         = 0,

        SoundFiles      = Ogg | Wave,

        [FileExtension("ogg")]
        Ogg             = 0x01,

        [FileExtension("wav")]
        Wave            = 0x02,

        [FileExtension("zlib")]
        Zlib            = 0x04,

        Graphics        = JPG | DXT5,

        DXT             = DXT1 | DXT3 | DXT5,

        DXT1            = 0x08,

        DXT3            = 0x10,

        DXT5            = 0x20,

        [FileExtension("jpg")]
        JPG             = 0x40,

        TGA             = 0x80,
    }
}
