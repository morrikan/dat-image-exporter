﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoK.DatUtil
{
    internal class DatConstants
    {
        public const uint MAX_FILES = 61;
        public const uint MAX_DIRS = 62;
        public const uint FILE_BLOCK_SIZE = 8 * sizeof(uint) * MAX_FILES;
        public const uint DIR_BLOCK_SIZE = 2 * sizeof(uint) * MAX_DIRS;
    }
}
