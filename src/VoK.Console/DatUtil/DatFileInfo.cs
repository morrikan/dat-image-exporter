﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoK.DatUtil
{
    internal class DatFileInfo
    {
        public int Id { get; set; }

        public int Size { get; set; }

        public int Iteration { get; set; }

        public int Version { get; set; }

        public override string ToString()
        {
            return $"{Id:X8} ({Size}, i: {Iteration}, v: {Version})";
        }
    }
}
