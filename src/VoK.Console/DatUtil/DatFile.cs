﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Newtonsoft.Json;
using VoK.Common;

namespace VoK.DatUtil
{
    internal class DatFile : IDisposable
    {
        private static volatile int _nextFileHandle = 1;
        private static object _fileHandleMutex = new object();

        [JsonProperty("datFileLocation")]
        public string DatFilePath { get; set; }

        [JsonProperty("internalFileListing")]
        public List<DatFileInfo> FileList { get; set; } = new List<DatFileInfo>();

        [JsonProperty("didMasterMap")]
        public int DidMasterMap { get; set; }

        [JsonProperty("blockSize")]
        public int BlockSize { get; set; }

        [JsonProperty("numDatFile")]
        public int NumDatFile { get; set; }

        [JsonProperty("game")]
        public GameId NumGameData { get; set; }

        [JsonProperty("datFileId")]
        public ulong DatFileId { get; set; }

        [JsonProperty("datIdStamp")]
        public string DatIdStamp { get; set; }

        /// <summary>
        /// content is a GUID.  Unknown usage.
        /// </summary>
        [JsonProperty("firstIteration")]
        public string FirstIteration { get; set; }

        private int _fileHandle;

        [JsonIgnore]
        public Dictionary<int, DatFileInfo> FileDictionary { get; set; } = null;

        private DatFile()
        {
        }

        public static DatFile LoadFromDatFile(string filePath)
        {
            DatFile df = new DatFile();
            if (!File.Exists(filePath))
                throw new FileNotFoundException("File not found", filePath);

            df.DatFilePath = filePath;
            df.GetFileHandle();

            StringBuilder datIdStamp = new StringBuilder();
            StringBuilder firstIteration = new StringBuilder();

            var flags = DatExportWrapper.Flags.ReadOnly
                         | DatExportWrapper.Flags.LoadIterations
                         | DatExportWrapper.Flags.Expandable;
           
            var openResult = DatExportWrapper.OpenDatFile(df._fileHandle,
                filePath,
                (uint)flags,
                out int didMasterMap,
                out int blockSize,
                out int numDatFile,
                out int numGameData,
                out ulong datFileId,
                datIdStamp,
                firstIteration);

            if (openResult != 1)
            {
                System.Console.WriteLine($"Unable to open dat file using DatExport.dll. Return code {openResult}");
                return null;
            }

            df.DidMasterMap = didMasterMap;
            df.BlockSize = blockSize;
            df.NumDatFile = numDatFile;
            df.NumGameData = (GameId)numGameData;
            df.DatFileId = datFileId;
            df.DatIdStamp = datIdStamp.ToString();
            df.FirstIteration = firstIteration.ToString();

            System.Console.WriteLine($"Dat File loaded:");
            System.Console.WriteLine($"  Filename: {filePath}");
            System.Console.WriteLine($"  didMasterMap: {didMasterMap:X8}");
            System.Console.WriteLine($"  blockSize: {blockSize}");
            System.Console.WriteLine($"  numDatFile: {numDatFile}");
            System.Console.WriteLine($"  numGameData: {numGameData}");
            System.Console.WriteLine($"  datFileId: {datFileId:X16}");
            System.Console.WriteLine($"  firstIteration: {firstIteration}");
            System.Console.WriteLine($"  datIdStamp: {datIdStamp}");

            int numSubFiles = DatExportWrapper.GetNumSubfiles(df._fileHandle);

            System.Console.WriteLine($"  numSubFiles: {numSubFiles}");

            int did;
            int size;
            int iteration;

            System.Console.WriteLine($"  attempting to load dat table of contents...");

            for (int i = 0; i < numSubFiles; i++)
            {
                System.Console.Write($"\r  Loading {i}...");

                try
                {
                    var subfileResult = DatExportWrapper.GetSubfileSizes(df._fileHandle, out did, out size, out iteration, i, 1);

                    if (subfileResult == 0)
                    {
                        System.Console.WriteLine($"Unable to get subfile {i} using DatExport.dll. Return code {subfileResult}");
                        continue;
                    }

                    var version = DatExportWrapper.GetSubfileVersion(df._fileHandle, did);
                    DatFileInfo dfi = new DatFileInfo() { Id = did, Size = size, Iteration = iteration, Version = version };
                    df.FileList.Add(dfi);
                }
                catch(Exception ex)
                {
                    System.Console.WriteLine($"Error loading file {i}: {ex}");
                    continue;
                }
            }

            // list is all set, build the dictionary
            df.FileDictionary = df.FileList.ToDictionary(i => i.Id, i => i);

            return df;
        }

        public static DatFile DeserializeFromJson(string filePath)
        {

            return null;
        }
        
        private void GetFileHandle()
        {
            lock (_fileHandleMutex)
            {
                _fileHandle = _nextFileHandle++;
            }
        }

        /// <summary>
        /// gets the contents of the requested file, decompressing them if needed.
        /// </summary>
        public byte[] GetFileContents(int fileId)
        {
            string ext;
            return GetFileContents(fileId, out ext);
        }

        /// <summary>
        /// gets the contents of the requested file, decompressing them if needed.
        /// </summary>
        public byte[] GetFileContents(int fileId, out string fileExtension)
        {
            if (!FileDictionary.ContainsKey(fileId))
                throw new ArgumentException("File Id not found in dat file.", nameof(fileId));

            int size = FileDictionary[fileId].Size;
            IntPtr ptr = new IntPtr();
            byte[] byteArray;
            int isCompressed = 0;
            fileExtension = ".bin";
            
            try
            {
                ptr = Marshal.AllocHGlobal(size);
                
                int result = DatExportWrapper.GetSubfileData(_fileHandle, fileId, ptr, 0, out int version);
                
                byteArray = new byte[size];
                Marshal.Copy(ptr, byteArray, 0, size);

                isCompressed = DatExportWrapper.GetSubfileCompressionFlag(_fileHandle, fileId);

            }
            catch (Exception ex)
            {
                System.Console.WriteLine("Unable to get contents for file " + fileId + Environment.NewLine + "Exception: " + ex.ToString());
                return null;
            }
            finally
            {
                Marshal.FreeHGlobal(ptr);
            }

            if (isCompressed != 0)
            {
                // remove the first 4 bytes of the array (the file id) and decompress the remainder
                var compressed = byteArray.Skip(4).ToArray();
                byteArray = Ionic.Zlib.ZlibStream.UncompressBuffer(compressed);
            }

            // check for file types we know about, and alter accordingly


            return byteArray;
        }

        public void Dispose()
        {
            DatExportWrapper.CloseDatFile(_fileHandle);
        }

        private void DoStuff(byte[] data)
        {

        }
    }
}
