using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using Microsoft.Win32;

namespace VoK
{
    internal class DatExportWrapper
    {
        [Flags]
        internal enum Flags : uint
        {
            UseLRU = 0x01,
            Expandable = 0x02,
            ReadOnly = 0x04,
            Create = 0x08,
            CreateIfNeeded = 0x10,
            OptionalFile = 0x20,
            FreeThreaded = 0x40,
            LoadIterations = 0x80,
            Journalled = 0x100,
            SkipIndexCheck = 0x200
        };

        [DllImport("DatExport.dll")]
        public static extern int CalculateFragmentation(int handle, out int numFiles, out int numFrags, out byte hint);

        [DllImport("DatExport.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void CloseDatFile(int handle);

        [DllImport("DatExport.dll", EntryPoint = "CreateDatFileEx")]
        public static extern int CreateDatFile(int handle, string fileName, int didMasterMap, int blockSize,
                                                int vnumDatFile, int vnumGameData, ulong datFileId, string datIdStamp,
                                                string firstIterGuid);

        //[DllImport("DatExport.dll")]
        //public static extern int GetNumIterations(int handle);

        [DllImport("DatExport.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GetNumSubfiles(int handle);

        [DllImport("DatExport.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte GetSubfileCompressionFlag(int handle, int did);

        [DllImport("DatExport.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GetSubfileData(int handle, int did, IntPtr buffer, int bufferOffset, out int version);

        //[DllImport("DatExport.dll", CallingConvention = CallingConvention.Cdecl)]
        //public static extern int GetSubfileDataFragment(int handle, int did, IntPtr buffer, int bufferOffset,
        //                                                    int readOffset, int length, out int version);

        [DllImport("DatExport.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GetSubfileSizes(int handle, out int did, out int size, out int iteration, int offset, int count);

        [DllImport("DatExport.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GetSubfileVersion(int handle, int did);

        [DllImport("DatExport.dll", EntryPoint = "OpenDatFileEx2", CallingConvention = CallingConvention.Cdecl)]
        public static extern int OpenDatFile(int handle, string fileName, uint flags, out int didMasterMap,
                                                out int blockSize, out int vnumDatFile, out int vnumGameData,
                                                out ulong datFileId,
                                                [MarshalAs(UnmanagedType.LPStr)] StringBuilder datIdStamp,
                                                [MarshalAs(UnmanagedType.LPStr)] StringBuilder firstIterGuid);

        [DllImport("DatExport.dll")]
        public static extern byte PreallocateSubfile(int handle, int did, int length, int version, int iteration, bool compressed);

        [DllImport("DatExport.dll")]
        public static extern int PutSubfileData(int handle, int did, IntPtr buffer, int offset, int length, int version,
                                                  int iteration, bool compressed);

        [DllImport("DatExport.dll")]
        public static extern int PutSubfileDataFragment(int handle, int id, IntPtr buffer, int sourceOffset,
                                                          int targetOffset, int length, int version, int iteration);
    }
}