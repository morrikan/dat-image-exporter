﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoK.DatUtil
{
    internal class FileExtensionAttribute : Attribute
    {
        public FileExtensionAttribute(string extension)
        {
            FileExtension = extension;
        }

        public string FileExtension { get; set; }
    }
}
