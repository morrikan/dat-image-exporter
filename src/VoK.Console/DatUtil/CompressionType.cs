﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoK.DatUtil
{
    internal enum CompressionType
    {
        Uncompressed = 0,
        Maximum = 1,
        Unknown_Value_2 = 2,
        Default = 3
    }
}
