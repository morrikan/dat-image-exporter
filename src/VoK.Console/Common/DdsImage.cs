using System;
using System.IO;
using System.Linq;

namespace VoK.Common
{
	public class DdsImage
	{
		private bool _isValid;
		private bool _alpha;
        private byte[] _pixelData;
        private byte[] _data;
        
		public bool IsValid
		{
			get { return _isValid; }
		}

		public bool PreserveAlpha
		{
			get { return _alpha; }
			set { _alpha = value; }
		}

	    public byte[] BitmapData
	    {
            get { return _data; }
	    }

	    public byte[] PixelData
	    {
	        get { return _pixelData; }
	    }

        public DdsImage(byte[] ddsImage, bool preserveAlpha = true)
		{
			if (ddsImage == null)
				return;

			if (ddsImage.Length == 0)
				return;

			_alpha = preserveAlpha;

			using (MemoryStream stream = new MemoryStream(ddsImage.Length))
			{
				stream.Write(ddsImage, 0, ddsImage.Length);
				stream.Seek(0, SeekOrigin.Begin);

				using (BinaryReader reader = new BinaryReader(stream))
				{
					Parse(reader);
				}
			}
		}

		public DdsImage(Stream ddsImage, bool preserveAlpha = true)
		{
			if (ddsImage == null)
				return;

			if (!ddsImage.CanRead)
				return;

			_alpha = preserveAlpha;

			using (BinaryReader reader = new BinaryReader(ddsImage))
			{
				Parse(reader);
			}
		}
        
		private void Parse(BinaryReader reader)
		{
			DDSStruct header = new DDSStruct();
			DdsPixelFormat pixelFormat = DdsPixelFormat.UNKNOWN;
			byte[] data = null;

			if (ReadHeader(reader, ref header))
			{
				_isValid = true;
				// patches for stuff
				if (header.depth == 0) header.depth = 1;

				uint blocksize = 0;
				pixelFormat = GetFormat(header, ref blocksize);
				if (pixelFormat == DdsPixelFormat.UNKNOWN)
				{
					throw new InvalidDataException("pixel format unrecognized");
				}

				data = ReadData(reader, header);
				if (data != null)
				{
				    _pixelData = Decompressor.Expand(header, data, pixelFormat);
                    _data = BmpUtil.GenerateBmp(ImageFormat.DDS, (int)header.width, (int)header.height, _pixelData);
                }
			}
		}
        
        private byte[] ReadData(BinaryReader reader, DDSStruct header)
		{
			byte[] compdata = null;
			uint compsize = 0;

			if ((header.flags & DdsUtils.DDSD_LINEARSIZE) > 1)
			{
				compdata = reader.ReadBytes((int)header.sizeorpitch);
				compsize = (uint)compdata.Length;
			}
			else
			{
				uint bps = header.width * header.pixelformat.rgbbitcount / 8;
				compsize = bps * header.height * header.depth;
				compdata = new byte[compsize];

				MemoryStream mem = new MemoryStream((int)compsize);

				byte[] temp;
				for (int z = 0; z < header.depth; z++)
				{
					for (int y = 0; y < header.height; y++)
					{
						temp = reader.ReadBytes((int)bps);
						mem.Write(temp, 0, temp.Length);
					}
				}
				mem.Seek(0, SeekOrigin.Begin);

				mem.Read(compdata, 0, compdata.Length);
				mem.Close();
			}

			return compdata;
		}
        
		private bool ReadHeader(BinaryReader reader, ref DDSStruct header)
		{
			byte[] signature = reader.ReadBytes(4);
			if (!(signature[0] == 'D' && signature[1] == 'D' && signature[2] == 'S' && signature[3] == ' '))
				return false;

			header.size = reader.ReadUInt32();
			if (header.size != 124)
				return false;

			//convert the data
			header.flags = reader.ReadUInt32();
			header.height = reader.ReadUInt32();
			header.width = reader.ReadUInt32();
			header.sizeorpitch = reader.ReadUInt32();
			header.depth = reader.ReadUInt32();
			header.mipmapcount = reader.ReadUInt32();
			header.alphabitdepth = reader.ReadUInt32();

			header.reserved = new uint[10];
			for (int i = 0; i < 10; i++)
			{
				header.reserved[i] = reader.ReadUInt32();
			}

			//pixelfromat
			header.pixelformat.size = reader.ReadUInt32();
			header.pixelformat.flags = reader.ReadUInt32();
			header.pixelformat.fourcc = reader.ReadUInt32();
			header.pixelformat.rgbbitcount = reader.ReadUInt32();
			header.pixelformat.rbitmask = reader.ReadUInt32();
			header.pixelformat.gbitmask = reader.ReadUInt32();
			header.pixelformat.bbitmask = reader.ReadUInt32();
			header.pixelformat.alphabitmask = reader.ReadUInt32();

			//caps
			header.ddscaps.caps1 = reader.ReadUInt32();
			header.ddscaps.caps2 = reader.ReadUInt32();
			header.ddscaps.caps3 = reader.ReadUInt32();
			header.ddscaps.caps4 = reader.ReadUInt32();
			header.texturestage = reader.ReadUInt32();

			return true;
		}

		private DdsPixelFormat GetFormat(DDSStruct header, ref uint blocksize)
		{
			DdsPixelFormat format = DdsPixelFormat.UNKNOWN;
			if ((header.pixelformat.flags & DdsUtils.DDPF_FOURCC) == DdsUtils.DDPF_FOURCC)
			{
				blocksize = ((header.width + 3) / 4) * ((header.height + 3) / 4) * header.depth;

				switch (header.pixelformat.fourcc)
				{
					case DdsUtils.FOURCC_DXT1:
						format = DdsPixelFormat.DXT1;
						blocksize *= 8;
						break;

					case DdsUtils.FOURCC_DXT2:
						format = DdsPixelFormat.DXT2;
						blocksize *= 16;
						break;

					case DdsUtils.FOURCC_DXT3:
						format = DdsPixelFormat.DXT3;
						blocksize *= 16;
						break;

					case DdsUtils.FOURCC_DXT4:
						format = DdsPixelFormat.DXT4;
						blocksize *= 16;
						break;

					case DdsUtils.FOURCC_DXT5:
						format = DdsPixelFormat.DXT5;
						blocksize *= 16;
						break;

					case DdsUtils.FOURCC_ATI1:
						format = DdsPixelFormat.ATI1N;
						blocksize *= 8;
						break;

					case DdsUtils.FOURCC_ATI2:
						format = DdsPixelFormat.THREEDC;
						blocksize *= 16;
						break;

					case DdsUtils.FOURCC_RXGB:
						format = DdsPixelFormat.RXGB;
						blocksize *= 16;
						break;

					case DdsUtils.FOURCC_DOLLARNULL:
						format = DdsPixelFormat.A16B16G16R16;
						blocksize = header.width * header.height * header.depth * 8;
						break;

					case DdsUtils.FOURCC_oNULL:
						format = DdsPixelFormat.R16F;
						blocksize = header.width * header.height * header.depth * 2;
						break;

					case DdsUtils.FOURCC_pNULL:
						format = DdsPixelFormat.G16R16F;
						blocksize = header.width * header.height * header.depth * 4;
						break;

					case DdsUtils.FOURCC_qNULL:
						format = DdsPixelFormat.A16B16G16R16F;
						blocksize = header.width * header.height * header.depth * 8;
						break;

					case DdsUtils.FOURCC_rNULL:
						format = DdsPixelFormat.R32F;
						blocksize = header.width * header.height * header.depth * 4;
						break;

					case DdsUtils.FOURCC_sNULL:
						format = DdsPixelFormat.G32R32F;
						blocksize = header.width * header.height * header.depth * 8;
						break;

					case DdsUtils.FOURCC_tNULL:
						format = DdsPixelFormat.A32B32G32R32F;
						blocksize = header.width * header.height * header.depth * 16;
						break;

					default:
						format = DdsPixelFormat.UNKNOWN;
						blocksize *= 16;
						break;
				}
			}
			else
			{
				// uncompressed image
				if ((header.pixelformat.flags & DdsUtils.DDPF_LUMINANCE) == DdsUtils.DDPF_LUMINANCE)
				{
					if ((header.pixelformat.flags & DdsUtils.DDPF_ALPHAPIXELS) == DdsUtils.DDPF_ALPHAPIXELS)
					{
						format = DdsPixelFormat.LUMINANCE_ALPHA;
					}
					else
					{
						format = DdsPixelFormat.LUMINANCE;
					}
				}
				else
				{
					if ((header.pixelformat.flags & DdsUtils.DDPF_ALPHAPIXELS) == DdsUtils.DDPF_ALPHAPIXELS)
					{
						format = DdsPixelFormat.RGBA;
					}
					else
					{
						format = DdsPixelFormat.RGB;
					}
				}

				blocksize = (header.width * header.height * header.depth * (header.pixelformat.rgbbitcount >> 3));
			}

			return format;
		}
	}
}