﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;

namespace VoK.Common
{
    internal class ProcessHelper
    {
        internal static string GetProcessName(GameId game)
        {
            MemberInfo memberInfo = typeof(GameId).GetMember(game.ToString()).FirstOrDefault();
            ProcessNameAttribute attr = (ProcessNameAttribute)memberInfo?.GetCustomAttribute(typeof(ProcessNameAttribute));
            return attr.Name;
        }
    }
}
