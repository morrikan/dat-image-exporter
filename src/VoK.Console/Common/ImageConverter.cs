﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;

namespace VoK.Common
{
    internal class ImageConverter
    {
        public static byte[] ConvertImage(int fileId, byte[] data, out string fileExtension, out ImageFormat imageFormat, out bool isIcon)
        {
            byte[] bmpData;
            byte[] pngData;

            fileExtension = ".bin";
            imageFormat = ImageFormat.Unknown;
            isIcon = false;

            if (data == null || data.Length < 24)
                return data;

            var dword0 = BitConverter.ToUInt32(data, 0);
            uint rawFormat = BitConverter.ToUInt32(data, 12);
            imageFormat = (ImageFormat)rawFormat;

            int width = BitConverter.ToInt32(data, 4);
            int height = BitConverter.ToInt32(data, 8);
            if (width == 36 && height == 36)
                isIcon = true;
            
            uint size = BitConverter.ToUInt32(data, 16);
            var rawData = data.Skip(20).ToArray();

            switch (imageFormat)
            {
                case ImageFormat.Dxt1:
                case ImageFormat.Dxt3:
                case ImageFormat.Dxt5:
                    var ddsData = DxtToDds(rawData, (uint)height, (uint)width, data[15]);
                    bmpData = DdsToBmp(ddsData);
                    pngData = BmpToPng(bmpData, width, height);
                    data = pngData;
                    fileExtension = ".png";
                    break;
                case ImageFormat.A8:
                case ImageFormat.R8G8B8A8:
                case ImageFormat.R8G8B8:
                    bmpData = BmpUtil.GenerateBmp(imageFormat, width, height, rawData);
                    pngData = BmpToPng(bmpData, width, height);
                    data = pngData;
                    fileExtension = ".png";
                    break;
                case ImageFormat.Jpeg:
                    data = data.Skip(20).ToArray();
                    fileExtension = ".jpg";
                    break;
                default:
                    System.Console.WriteLine($"Unimplemented Graphics format {imageFormat} found in file {fileId}");
                    break;
            }

            return data;
        }

        private static byte[] BmpToPng(byte[] data, int width, int height)
        {
            byte[] pngData = null;
            int stride = width * 4;
            PixelFormat pf = PixelFormat.Format32bppArgb;

            var imageData = data.Skip(54).ToArray();

            using (MemoryStream bmpStream = new MemoryStream(data))
            {
                unsafe
                {
                    fixed (byte* ptr = imageData)
                    {
                        using (Bitmap image = new Bitmap(width, height, stride, PixelFormat.Format32bppArgb, new IntPtr(ptr)))
                        {
                            using (MemoryStream pngStream = new MemoryStream())
                            {
                                image.Save(pngStream, System.Drawing.Imaging.ImageFormat.Png);
                                pngData = pngStream.GetBuffer();
                            }
                        }
                    }
                }

                //using (Bitmap bmp = new Bitmap(bmpStream))
                //{
                //    bmp.MakeTransparent(Color.Transparent);
                //    using (MemoryStream pngStream = new MemoryStream())
                //    {
                //        bmp.Save(pngStream, System.Drawing.Imaging.ImageFormat.Png);
                //        pngData = pngStream.GetBuffer();
                //    }
                //}
            }

            //using (var bmp = Image.Load(data, new SixLabors.ImageSharp.Formats.Bmp.BmpDecoder()))
            //{
            //    using (MemoryStream output = new MemoryStream())
            //    {
            //        bmp.SaveAsPng(output);
            //        pngData = output.GetBuffer();
            //    }
            //}

            return pngData;
        }

        private static byte[] DxtToDds(byte[] data, uint height, uint width, byte dxtIdByte)
        {
            byte[] dds = new byte[128 + data.Length];
            dds[0] = 0x44; // "D"
            dds[1] = 0x44; // "D"
            dds[2] = 0x53; // "S"
            dds[3] = 0x20; // " "

            dds[4] = 124; // size of remaining header

            // header flags
            dds[8] = 0x07;
            dds[9] = 0x10;
            dds[10] = 0x0A;

            var widthBytes = BitConverter.GetBytes(width);
            var heightBytes = BitConverter.GetBytes(height);
            var pitchBytes = BitConverter.GetBytes((uint)data.Length);

            Array.Copy(widthBytes, 0, dds, 12, 4);
            Array.Copy(heightBytes, 0, dds, 16, 4);
            Array.Copy(pitchBytes, 0, dds, 20, 4);

            dds[28] = 1; // mipmap count
            dds[76] = 32; // size of sub-struct, static
            dds[80] = 4; // DDPF_FOURCC

            dds[84] = 68; // "D"
            dds[85] = 88; // "X"
            dds[86] = 84; // "T"
            dds[87] = dxtIdByte; // 1, 3, or 5, as a string

            // DDSCAPS flags
            dds[108] = 8; // DDSCAPS_COMPLEX
            dds[109] = 16; // DDSCAPS_TEXTURE
            dds[110] = 64; // DDSCAPS_MIPMAP

            Array.Copy(data, 0, dds, 128, data.Length);
            return dds;
        }


        private static byte[] DdsToBmp(byte[] data)
        {
            DdsImage img = new DdsImage(data);
            return img.BitmapData;
        }
    }
}
