﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoK.Common
{
    [AttributeUsage(AttributeTargets.Field)]
    internal class ProcessNameAttribute : Attribute
    {
        public ProcessNameAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}
