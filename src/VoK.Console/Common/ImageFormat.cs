﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoK.Common
{
    public enum ImageFormat
    {
        Unknown             = 0x00000000,
        R8G8B8              = 0x00000014,
        R8G8B8A8            = 0x00000015,
        
        A8                  = 0x0000001C, // unknown, alpha is likely.  common in the 0x4100B range of client_surface.  

        Jpeg                = 0x000001F4,

        DDS                 = 0x20534444, // "DDS "

        Dxt1                = 0x31545844, // "DXT1"
        Dxt2                = 0x32545844, // "DXT2"
        Dxt3                = 0x33545844, // "DXT3"
        Dxt4                = 0x34545844, // "DXT4"
        Dxt5                = 0x35545844, // "DXT5"
    }
}
