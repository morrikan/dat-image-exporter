﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoK.Common
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true)]
    internal class IdRangeAttribute : Attribute
    {
        public uint MinValue { get; private set; }

        public uint MaxValue { get; private set; }

        public IdRangeAttribute(uint min, uint max)
        {
            MinValue = min;
            MaxValue = max;
        }
    }
}
