﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace VoK.Common
{
    internal class SoundConverter
    {
        private const uint RIFF = 0x46464952; // "RIFF"
        private const uint WAVE = 0x45564157; // "WAVE"
        private const uint OGG = 0x5367674F; // "OggS" for Ogg-Vorbis sound clip

        public static byte[] Convert(int fileId, byte[] data, out string fileExtension)
        {
            fileExtension = ".bin";

            if (data == null || data.Length < 20)
                return data;

            // first dword is the length of the file, which we don't need
            data = data.Skip(4).ToArray();

            // the first dword is audio type.
            uint audioType = BitConverter.ToUInt32(data, 0);
            byte[] mp3Data = null;
            string tempRaw = null;
            string tempMp3 = null;
            
            switch (audioType)
            {
                case RIFF:
                    uint riffType = BitConverter.ToUInt32(data, 8);

                    if (riffType == WAVE)
                    {
                        tempRaw = Path.GetTempFileName() + ".wav";
                        tempMp3 = Path.GetTempFileName() + ".mp3";
                        File.WriteAllBytes(tempRaw, data);
                        
                        try
                        {
                            Process p = new Process { StartInfo = new ProcessStartInfo("ffmpeg.exe", $"-i {tempRaw} {tempMp3}") { CreateNoWindow = true, UseShellExecute = false } };
                            p.Start();
                            p.WaitForExit();

                            data = File.ReadAllBytes(tempMp3);
                            File.Delete(tempRaw);
                            File.Delete(tempMp3);

                            fileExtension = ".mp3";
                            return data;
                        }
                        catch (Exception ex)
                        {
                            System.Console.WriteLine($"Error converting {fileId}: {ex}");
                        }
                    }

                    System.Console.WriteLine($"Unknown RIFF format {riffType} found in file {fileId}");
                    break;

                case OGG:
                    byte oggVersion = data[5];
                    byte headerVersion = data[6];

                    tempRaw = Path.GetTempFileName() + ".ogg";
                    tempMp3 = Path.GetTempFileName() + ".mp3";
                    File.WriteAllBytes(tempRaw, data);
                    
                    try
                    {
                        Process p = new Process { StartInfo = new ProcessStartInfo("ffmpeg.exe", $"-i {tempRaw} {tempMp3}") { CreateNoWindow = true, UseShellExecute = false } };
                        p.Start();
                        p.WaitForExit();

                        data = File.ReadAllBytes(tempMp3);
                        File.Delete(tempRaw);
                        File.Delete(tempMp3);

                        fileExtension = ".mp3";
                    }
                    catch (Exception ex)
                    {
                        System.Console.WriteLine($"Error converting {fileId}: {ex}");
                    }

                    break;

                default:
                    System.Console.WriteLine($"Unimplemented Sound format {audioType} found in file {fileId}");
                    break;
            }

            return data;
        }
    }
}
