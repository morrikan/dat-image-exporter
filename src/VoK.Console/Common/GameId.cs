﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoK.Common
{
    /// <summary>
    /// enumeration values match the identifiers found in the dat files
    /// </summary>
    internal enum GameId
    {
        [ProcessName("acclient")]
        AsheronsCall    = 1,

        [ProcessName("ac2client")]
        AsheronsCall2   = 2,

        [ProcessName("dndclient")]
        DDO             = 3,

        [ProcessName("lotroclient")]
        LOTRO           = 4,
    }
}
