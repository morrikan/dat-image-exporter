﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace VoK.Common
{
    internal class BmpUtil
    {
        public static byte[] GenerateBmp(ImageFormat format, int width, int height, byte[] rawData)
        {
            // note: Bitmap stores it's data in RGBA order.

            byte[] result = null;
            uint imageSize = (uint)(width * height * 4);
            uint totalSize = imageSize + 54;
            uint dataStart = 54;
            uint secondHeaderSize = 40;
            ushort bitsPerPixel = 32;
            
            using (MemoryStream output = new MemoryStream((int)totalSize))
            {
                using (var writer = new BinaryWriter(output))
                {
                    writer.Write((ushort)19778);            // static file header
                    writer.Write(totalSize);                // total size of the file
                    writer.Write((uint)0);
                    writer.Write(dataStart);                // where the data starts
                    writer.Write(secondHeaderSize);         // size of the second header to follow
                    writer.Write(width);
                    writer.Write(height);
                    writer.Write((ushort)1);                // number of color planes
                    writer.Write((ushort)bitsPerPixel);     // bits per pixel
                    writer.Write((uint)0);                  // compression method
                    writer.Write((uint)imageSize);
                    writer.Write((uint)0);                  // X-pixels per meter
                    writer.Write((uint)0);                  // Y-pixels per meter
                    writer.Write((uint)0);
                    writer.Write((uint)0);

                    switch (format)
                    {
                        case ImageFormat.DDS:
                            {
                                byte[] buffer = new byte[imageSize];

                                for (int h = 0; h < height; h++)
                                {
                                    for (int w = 0; w < width; w++)
                                    { 
                                        int sourceCell = h * width * 4 + w * 4;
                                        int targetCell = h * width * 4 + w * 4;

                                        buffer[targetCell + 0] = rawData[sourceCell + 2]; // blue
                                        buffer[targetCell + 1] = rawData[sourceCell + 1]; // green
                                        buffer[targetCell + 2] = rawData[sourceCell + 0]; // red
                                        buffer[targetCell + 3] = rawData[sourceCell + 3]; // alpha
                                    }

                                    // at 4-bytes per pixel, there's never any padding issues
                                }

                                writer.Write(buffer);
                                break;
                            }
                        case ImageFormat.R8G8B8A8:
                            {
                                byte[] buffer = new byte[imageSize];

                                for (int h = 0; h < height; h++)
                                {
                                    for (int w = 0; w < width; w++)
                                    { 
                                        int sourceCell = h * width * 4 + w * 4;
                                        int targetCell = h * width * 4 + w * 4;

                                        buffer[targetCell + 0] = rawData[sourceCell + 0]; // red
                                        buffer[targetCell + 1] = rawData[sourceCell + 1]; // green
                                        buffer[targetCell + 2] = rawData[sourceCell + 2]; // blue
                                        buffer[targetCell + 3] = rawData[sourceCell + 3]; // alpha
                                    }

                                    // at 4-bytes per pixel, there's never any padding issues
                                }

                                writer.Write(buffer);
                                break;
                            }
                        case ImageFormat.R8G8B8:
                            {
                                byte[] buffer = new byte[imageSize];

                                for (int h = 0; h < height; h++)
                                {
                                    for (int w = 0; w < width; w++)
                                    {
                                        int sourceCell = h * width * 3 + w * 3;
                                        int targetCell = h * width * 4 + w * 4;

                                        buffer[targetCell + 0] = rawData[sourceCell + 0]; // red
                                        buffer[targetCell + 1] = rawData[sourceCell + 1]; // green
                                        buffer[targetCell + 2] = rawData[sourceCell + 2]; // blue
                                        buffer[targetCell + 3] = 0xFF; // alpha
                                    }
                                }

                                writer.Write(buffer);
                                break;
                            }

                        case ImageFormat.A8:
                            {
                                // in order to make this visible, we're going to make this a 32-bit image
                                // using black as the only color, with an alpha blend
                                byte[] buffer = new byte[imageSize];

                                for (int h = 0; h < height; h++)
                                {
                                    for (int w = 0; w < width; w++)
                                    {
                                        int sourceCell = h * width + w;
                                        int targetCell = h * width * 4 + w * 4;

                                        buffer[targetCell + 0] = 0x00; // red
                                        buffer[targetCell + 1] = 0x00; // green
                                        buffer[targetCell + 2] = 0x00; // blue
                                        buffer[targetCell + 3] = rawData[sourceCell]; // alpha
                                    }
                                }

                                writer.Write(buffer);
                                break;
                            }
                    }
                }

                result = output.GetBuffer();
            }

            return result;
        }
    }
}
