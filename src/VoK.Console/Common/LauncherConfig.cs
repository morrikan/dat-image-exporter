﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.XPath;

namespace VoK.Common
{
    internal class LauncherConfig
    {
        private string _localPath;
        private string _installPath;

        private readonly Dictionary<string, string> _launcherSettings;

        public LauncherConfig(string installPath)
        {
            string launcherConfig = Path.Combine(installPath, "TurbineLauncher.exe.config");

            if (!File.Exists(launcherConfig))
                throw new ArgumentException("Install path is not valid.");

            _installPath = installPath;
            _localPath = launcherConfig;

            XPathDocument doc = new XPathDocument(launcherConfig);
            var nav = doc.CreateNavigator();
            var settingNodes = nav.Select("/configuration/appSettings/add");

            _launcherSettings = new Dictionary<string, string>();
            foreach (XPathNavigator node in settingNodes)
            {
                _launcherSettings.Add(node.GetAttribute("key", ""), node.GetAttribute("value", ""));
            }
        }

        public string Game => _launcherSettings["DataCenter.GameName"];
    }
}
