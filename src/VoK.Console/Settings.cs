﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace VoK.Console
{
    public class Settings
    {
        [JsonProperty("installationDirectories")]
        public List<string> InstallationDirectories { get; set; } = new List<string>();

        [JsonProperty("outputFolder")]
        public string OutputFolder { get; set; }
    }
}
